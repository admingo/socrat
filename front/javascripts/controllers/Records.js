define(['./module', 'jquery'], function (controllers, $) {
    'use strict';
    controllers.controller('Records', ['$scope', '$state', '$stateParams', 'ngSocket', function ($scope, $state, $stateParams, ngSocket) {

        $scope.goPreviousPage = function () {
            if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
            else $state.go('records', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset - limit
            });
        };
        $scope.goNextPage = function () {
            if (offset >= ($scope.pagination.length - 1) * limit) return false;
            $state.go('records', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset + limit
            });
        };

        // var limit = 20;

        // объявление переменных необходимых для сортировки и выборки
        var offset = parseInt($stateParams.offset == null ? 0 : $stateParams.offset);
        var limit = 5;
        $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
        $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};


        //
        $scope.sendRequest = function () {
            ngSocket.emit('getRecordList', {
                limit: limit,
                offset: offset,
                filter: $scope.sendSearchRequest,
                sort: $scope.sortParams
            });
        };
        $scope.setSort = function (key) {
            if ($scope.sortParams[key] >= 2 || $scope.sortParams[key] == null) {
                $scope.sortParams[key] = 0;
            } else {
                $scope.sortParams[key]++;
            }
            $scope.sendRequest();
        };
        $scope.resetData = function () {
            $scope.sendSearchRequest.begin_ts = ' ';
            $scope.goToState(offset);
        };
        $scope.sendFilter = function (e) {
            if (e.keyCode === 13) {
                offset = 0;
                $scope.goToState();
            }

        };
        $scope.goToState = function () {
            offset = arguments.length ? arguments[0] : offset;
            $state.go($state.current.name, {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        };
        ngSocket.emit('getRecordList', {limit: limit, filter: $scope.sendSearchRequest, offset: offset});
        ngSocket.on('recordList', function (data) {
            if (data.err) {
                alert(data.message);
                return false
            }
            $scope.records = data.data.rows;
            var countPages = Math.floor((data.data.count + limit - 1) / limit);
            $scope.pagination = [];
            for (var i = 0; i < countPages; i++) {
                $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
            }
        });

        $scope.sr = {};
        $scope.sr.lessonId = parseInt($scope.sendSearchRequest.lessonId);
        $scope.sr.classId = parseInt($scope.sendSearchRequest.classId);
        $scope.sr.userId = parseInt($scope.sendSearchRequest.userId);
        $scope.$on('$viewContentLoading', function () {
            $scope.$on('dropDownNewValue', function () {
                if ($scope.$state.current.name === 'records') {
                    setTimeout(function () {
                        if ($scope.sr.lessonId !== $scope.sendSearchRequest.lessonId) {
                            $scope.sendSearchRequest.lessonId = +$scope.sr.lessonId;
                            $scope.goToState();
                        }
                        if ($scope.sr.classId !== $scope.sendSearchRequest.classId) {
                            $scope.sendSearchRequest.classId = +$scope.sr.classId;
                            $scope.goToState();
                        }
                    }, 5);
                }
            });

        });

        ngSocket.emit('getLessonList', {});
        ngSocket.emit('getClassList', {});
        ngSocket.emit('getUserList', {});
        ngSocket.on('classList', function (data) {
            if (data.err) {
                alert(data.message)
            }
            $scope.class = {0: 'Все классы'};
            data.data.rows.forEach(function (elem) {
                $scope.class[elem.id] = elem.className
            })

        });

        ngSocket.on('lessonsList', function (data) {
            if (data.err) {
                alert(data.message)
            }
            $scope.lesson = {0: 'Все предметы'};
            data.lessons.rows.forEach(function (elem) {
                $scope.lesson[elem.id] = elem.lessonName
            });
        });
        ngSocket.on('userListSelected', function (data) {
            if (data.err) {
                alert(data.message)
            }
            $scope.user = {0: 'Все преподователи'};
            data.users.rows.forEach(function (elem) {
                $scope.user[elem.id] = elem.lastName + ' ' + elem.firstName + ' ' + elem.patronymic
            });
        });










        //
        // $scope.goPreviousPage = function () {
        //     if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
        //     else $state.go('schedule', {
        //         sort: JSON.stringify($scope.sortParams),
        //         filter: JSON.stringify($scope.sendSearchRequest),
        //         offset: offset - limit
        //     });
        // };
        // $scope.goNextPage = function () {
        //     if (offset >= ($scope.pagination.length - 1) * limit) return false;
        //     $state.go('schedule', {
        //         sort: JSON.stringify($scope.sortParams),
        //         filter: JSON.stringify($scope.sendSearchRequest),
        //         offset: offset + limit
        //     });
        // };
        // // объявление переменных необходимых для сортировки и выборки
        // $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
        // $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};
        // let offset = parseInt($stateParams.offset == null ? 0 : $stateParams.offset);
        // let limit = 4;
        //
        // $scope.showNoteUpdated = $stateParams.showNoteUpdated;
        // $scope.sr = {};
        // $scope.sr.lessonId = parseInt($scope.sendSearchRequest.lessonId);
        // $scope.sr.classId = parseInt($scope.sendSearchRequest.classId);
        // $scope.sr.periodId = parseInt($scope.sendSearchRequest.periodId);
        // $scope.$on('$viewContentLoading', function () {
        //
        //     $scope.$on('dropDownNewValue', function () {
        //         if ($scope.$state.current.name === 'schedule') {
        //             setTimeout(function() {
        //                 if ($scope.sr.lessonId !== $scope.sendSearchRequest.lessonId) {
        //                     $scope.sendSearchRequest.lessonId = +$scope.sr.lessonId;
        //                     $scope.goToState();
        //                 }
        //                 if ($scope.sr.classId !== $scope.sendSearchRequest.classId) {
        //                     $scope.sendSearchRequest.classId = +$scope.sr.classId;
        //                     $scope.goToState();
        //                 }
        //                 if ($scope.sr.periodId !== $scope.sendSearchRequest.periodId) {
        //                     $scope.sendSearchRequest.periodId = +$scope.sr.periodId;
        //                     $scope.goToState();
        //                 }
        //             },5);
        //         }
        //     });
        //
        // });
        //
        //
        // $scope.sendRequest = function () {
        //     ngSocket.emit('getScheduleList', {
        //         limit: limit,
        //         offset: offset,
        //         filter: $scope.sendSearchRequest,
        //         sort: $scope.sortParams
        //     });
        // };
        //
        //
        // $scope.sendFilter = function (e) {
        //     if (e.keyCode === 13) {
        //         $scope.goToState();
        //     }
        // };
        // $scope.setSort = function (key) {
        //     if ($scope.sortParams[key] >= 2 || $scope.sortParams[key] == null) {
        //         $scope.sortParams[key] = 0;
        //     } else {
        //         $scope.sortParams[key]++;
        //     }
        //     $scope.sendRequest();
        // };
        // $scope.resetData = function () {
        //     $scope.sendSearchRequest.begin_ts = ' ';
        //     $scope.goToState(offset);
        // };
        //
        // $scope.goToState = function () {
        //     offset = arguments.length ? arguments[0] : offset;
        //     $state.go($state.current.name, {
        //         sort: JSON.stringify($scope.sortParams),
        //         filter: JSON.stringify($scope.sendSearchRequest),
        //         offset: offset
        //     });
        // };
        //
        // ngSocket.on('scheduleList', function (result) {
        //     if (result.err) {
        //         alert(result.message);
        //         return false
        //     }
        //     $scope.schedules = result.data.rows;
        //     var countPages = Math.round((result.data.count + 1) / limit);
        //     $scope.pagination = [];
        //     for (var i = 0; i < countPages; i++) {
        //         $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
        //     }
        // });
        //
        //
        //
        //


    }]).controller('RecordSaved', ['$scope', '$stateParams', 'ngSocket', function ($scope, $stateParams, ngSocket) {
        $scope.editRecordSucsessful = $stateParams.editRecordSucsessful;

    }]).controller('RecordsView', ['$scope', '$interval', '$rootScope', '$stateParams', 'ngSocket', function ($scope, $interval, $rootScope, $stateParams, ngSocket) {
        ngSocket.on('editVideo', function () {
            $scope.edit = true;
        });
        ngSocket.emit('getRecord', {id: $stateParams.id});
        
        ngSocket.on('catchRecord', function (data) {
            if (data.err) {
                alert(data.message);
            }
            var d = (data.data.event_ts.split('T'))[0].split('-');
            $scope.dateFormat = d[2]+'.'+d[1]+'.'+d[0];
            ngSocket.emit('getFileList', {scheduleId: data.data.scheduleId});
            $scope.videos = data.data.videos;
            $scope.info = data.data;
            
        });


        ngSocket.on('fileList', function (files) {
            if(files.err) {
                alert(files.message)
            }
            $scope.filelist = files.files;
        });

        var pl = null;
        $scope.videoResize = function () {
            // $scope.height = 100;
            // $scope.width = 100;
            $scope.resize = true;
        };
        $scope.videoResizeout = function () {
            $scope.resize = false;
        };
        $scope.playvideo = function () {
            $scope.played = !$scope.played;
            var player = $('#mainScreen video')[0];
            if ($scope.played) {
                player.play();
                if (pl !== null) {
                    $interval.cancel(pl);
                }
                pl = $interval(updateProgressbar, 10);
            } else {
                player.pause();
                $interval.cancel(pl);
            }
            function updateProgressbar() {
                var duration = player.duration; // длительность контента в секундах
                var currentTime = player.currentTime;
                var proportion = (currentTime / duration); // соотношение времени воспроизведения к длительности видео
                $scope.playWidth = ( 100 * proportion ) + '%';
                if (proportion == 1) {
                    $interval.cancel(pl);
                }
            }
        };

        $scope.swap = function (e) {
            $("#mainScreen").html($(e.currentTarget).parent().html());
            $scope.playvideo();
            $scope.playWidth = 0;
        };

        $scope.videoRewind = function (e) {

            // var x = e.offsetX==undefined?e.layerX:e.offsetX;
            // var locationCursor = ( (x*100)/(e.currentTarget.offsetWidth) ); // положение курсора в div-е в %

            var player = $('#mainScreen video')[0];

            var duration = player.duration; // длительность контента в секундах

            // var currentTime = ( 100 * (player.currentTime) / duration ); // текущее значение времени воспроизведения в %
            var currentTime = player.currentTime; // текущее значение времени воспроизведения в секундах

            var locationCursorX = e.offsetX == undefined ? e.layerX : e.offsetX; // положение курсора в div-е в пикселях

            var positionCursorInSecond = ( ( locationCursorX / e.currentTarget.offsetWidth ) * duration );
            // расчёт секунды, в зав-ти от положения курсора

            player.currentTime = ( positionCursorInSecond ); // перемотка на позицию положения курсора

        };

        $scope.soundOff = function () { // звук вкл/выкл
            var player = $('#mainScreen video')[0];
            $scope.volumed = !$scope.volumed;
            $scope.volumed ? player.volume = 0 : player.volume = 1;
            // player.volume = 1; // включаем полную громкость (для выключения
            // player.volume = 0; // звука полю нужно присвоить значение “0”)
        };


        $scope.changeVolume07 = function (e) {
            e.stopPropagation();
            var volume07 = $('#volume07').val();
            var video = $('#mainScreen video')[0];
            // console.log(volume07);
            video.volume = volume07 / 100;
        };

        $scope.soundChange = function (e) { // изменение громкости кликом мыши
            e.preventDefault();
            e.stopPropagation();
            var player = $('#mainScreen video')[0];
            var height = e.currentTarget.offsetHeight; // высота div-а с громкостью в пикселях

            var locationCursorY = height - (e.offsetY == undefined ? e.layerY : e.offsetY); // положение курсора в div-е в пикселях
            $('.progressSound-play-grey-blue').height(locationCursorY);
            $scope.playHeight = ( locationCursorY + 'px'); // передаём значение height в html

            var positionСursorSound = $('.progressSound-play-grey-blue').height() / $('.progressSound-play-grey').height(); // расчёт громкости, в зав-ти от положения курсора

            player.volume = positionСursorSound; // включение громкости на позицию положения курсора

            // console.log ( player.volume );
            // console.log ( ( locationCursorY  * 100 / height ) +'%' );

            //alert ( 'высота div-а с громкостью в пикселях = ' + height );
            //alert ( 'положение курсора в div-е в пикселях = ' + locationCursorY );
            //alert ( 'расчёт громкости, в зав-ти от положения курсора = ' + positionСursorSound);
            //alert ( 'включение громкости на позицию положения курсора = ' + positionСursorSound);
            //alert ( ' передаём значение height в html = ' + playHeight);

            return false;
        };

    }])
});

