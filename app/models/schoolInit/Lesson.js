/**
 * Created by piligrim on 01.06.16.
 */
var Sequelize = require('sequelize');

var attributes = {
    lessonName: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    description: {
        type: Sequelize.STRING
    },
    theme : {
        type: Sequelize.STRING
    },
    begin_ts : {
        type : Sequelize.DATE
    },
    end_ts : {
        type : Sequelize.DATE
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    }

};

module.exports.attributes = attributes;